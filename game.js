define(function(){
	var GameSettings = {
		name : 'AppName',
		version : '1.0.0'
	};

	/* DEV */
	if (document.location.hostname === 'localhost'){
		// GameSettings.apiURL = 'http://localhost:8000/appName';					// DEV
		GameSettings.apiURL = 'http://madjoh.com/api/web/app_dev.php/appName';		// TEST

		GameSettings.urlPrefix = '';
		GameSettings.facebook = false;

	/* PROD */
	}else{
		GameSettings.apiURL = 'http://madjoh.com/appName';
		GameSettings.urlPrefix = 'http://madjoh.com/Games/AppName/';
		GameSettings.facebook = true;
	}

	return GameSettings;
});